﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for converting a battle result into xp to be awarded to the player.
/// 
/// TODO:
///     Respond to battle outcome with xp calculation based on;
///         player win 
///         how strong the win was
///         stats/levels of the dancers involved
///     Award the calculated XP to the player stats
///     Raise the player level up event if needed
/// </summary>
public class XPHandler : MonoBehaviour
{
    private void OnEnable()
    {

    }

    private void OnDisable()
    {

    }

    public void GainXP(BattleResultEventData data /*float outcome*/)
    {
        Debug.Log("Gain xp has been called!");

        //This scriptis trash. Sincerely H-E
        //Crazy math

        //Stats.xp += outcome;

        // Need to check to see if the outcome from the data is greater than 0, if yes player has won
        // We then need to generate XP somehow, we could manipulate the amount of xp based on the outcome i.e. -1,0,1
        // We then need to call data.player.levelUp(pass in some XP);
        // we could expand this further to let npc level up of to do other things if it's a draw i.e. 0
    }
}
